package org.campus02.nowa;

import java.util.Collections;

import org.campus02.nowa.domain.Address;
import org.campus02.nowa.domain.AddressMobileEmailComparator;
import org.campus02.nowa.exception.AddressExportException;
import org.campus02.nowa.exception.AddressExportFileAlreadyExistsException;
import org.campus02.nowa.exception.AddressLoadException;
import org.campus02.nowa.exception.AddressLoadWrongFormatException;
import org.campus02.nowa.model.AddressManager;

public class DemoApp {
	public static void main(String[] args) {
		Address a1 = new Address("Max", "Muster", "06641234567", "max.muster@test.com");
		
		AddressManager am = new AddressManager();
	
		
		try {
			am.loadFromCsv("C:\\devdata\\PR2\\address.csv", ",");
			
			am.pintAll();
			
			am.add(a1);
			
			am.pintAll();
			
			am.exportToCsv("C:\\devdata\\PR2\\address2.csv", ",");
			
			
		} catch (AddressLoadWrongFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  catch (AddressExportException e1) {
			e1.printStackTrace();
		} catch (AddressExportFileAlreadyExistsException e2) {
			e2.printStackTrace();
		} catch (AddressLoadException e3) {
			e3.printStackTrace();
		}
		
		
		am.pintAll();
		
		Collections.sort(am.getAddresses());
		
		am.pintAll();
		
		Collections.sort(am.getAddresses(),new AddressMobileEmailComparator());
		
		am.pintAll();
	}
}
