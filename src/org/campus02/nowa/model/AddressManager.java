package org.campus02.nowa.model;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.campus02.nowa.domain.Address;
import org.campus02.nowa.exception.AddressExportException;
import org.campus02.nowa.exception.AddressExportFileAlreadyExistsException;
import org.campus02.nowa.exception.AddressLoadException;
import org.campus02.nowa.exception.AddressLoadWrongFormatException;

public class AddressManager {

	private List<Address> addresses;

	public AddressManager() {
		addresses = new ArrayList<>();
	}

	/**
	 * 
	 * @param address
	 */
	public void add(Address address) {
		addresses.add(address);
	}

	/**
	 * 
	 * @param path
	 * @param separator
	 * @throws AddressExportFileAlreadyExistsException
	 * @throws AddressExportException
	 */
	public void exportToCsv(String path, String separator)
			throws AddressExportFileAlreadyExistsException, AddressExportException {

		File file = new File(path);

		if (file.exists()) {
			throw new AddressExportFileAlreadyExistsException();
		}

		try (BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {

			for (Address address : addresses) {
				bw.write(address.toLine(separator));
				bw.newLine();
			}
			bw.flush();
		} catch (FileNotFoundException ex) {
			throw new AddressExportException(ex);
		} catch (IOException e) {
			throw new AddressExportException(e);
		}
	}

	/**
	 * 
	 * @param path
	 * @param separator
	 * @throws AddressLoadWrongFormatException
	 * @throws AddressExportException
	 * @throws AddressLoadException 
	 */
	public void loadFromCsv(String path, String separator)
			throws AddressLoadWrongFormatException, AddressExportException, AddressLoadException {
		File file = new File(path);

		try (BufferedReader br = new BufferedReader(new FileReader(file))) {

			String line;
			while ((line = br.readLine()) != null) {

				String[] columns = line.split(separator);

				if (columns.length != 4) {
					throw new AddressLoadWrongFormatException("Fehler bei Zeile: " + line);
				}
				Address address = new Address(columns[0], columns[1], columns[2], columns[3]);

				add(address);
			}

		} catch (FileNotFoundException e) {
			throw new AddressLoadException (e);
		} catch (IOException e1) {
			throw new AddressLoadException (e1);
		}
	}

	/**
	 * 
	 */
	public void pintAll() {
		System.out.println("Start printing -------- ");
		for (Address address : addresses) {
			System.out.println(address);
		}
		System.out.println("finish printing -------- ");
		System.out.println();

	}

	public List<Address> getAddresses() {
		return addresses;
	}

	public void setAddresses(List<Address> addresses) {
		this.addresses = addresses;
	}

}
