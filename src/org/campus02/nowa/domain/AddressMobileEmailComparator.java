package org.campus02.nowa.domain;

import java.util.Comparator;

public class AddressMobileEmailComparator implements Comparator<Address>{

	@Override
	public int compare(Address o1, Address o2) {
		int comp;
		if ((comp = o1.getMobilNumber()
				.compareTo(o2.getMobilNumber()))!= 0)
				return comp;
		if ((comp = o1.getEmail()
				.compareTo(o2.getEmail()))!= 0)
				return comp;
		return 0;
	}

}
