package org.campus02.nowa.exception;

public class AddressLoadException extends Exception{

	private static final long serialVersionUID = 3101809049583123426L;

	public AddressLoadException() {
		super();
	}

	public AddressLoadException(String message, Throwable cause) {
		super(message, cause);
	}

	public AddressLoadException(String message) {
		super(message);
	}

	public AddressLoadException(Throwable cause) {
		super(cause);
	}

}
