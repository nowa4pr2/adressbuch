package org.campus02.nowa.exception;

public class AddressLoadWrongFormatException extends Exception{

	public AddressLoadWrongFormatException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public AddressLoadWrongFormatException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public AddressLoadWrongFormatException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public AddressLoadWrongFormatException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	private static final long serialVersionUID = 3790275015698562561L;

}
