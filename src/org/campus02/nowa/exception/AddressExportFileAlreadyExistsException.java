package org.campus02.nowa.exception;

public class AddressExportFileAlreadyExistsException extends Exception{


	private static final long serialVersionUID = 1814681094403260598L;

	public AddressExportFileAlreadyExistsException() {
		super();
	}

	public AddressExportFileAlreadyExistsException(String message, Throwable cause) {
		super(message, cause);
	}

	public AddressExportFileAlreadyExistsException(String message) {
		super(message);
	}

	public AddressExportFileAlreadyExistsException(Throwable cause) {
		super(cause);
	}


}
