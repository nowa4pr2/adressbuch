package org.campus02.nowa.exception;

public class AddressExportException extends Exception {

	private static final long serialVersionUID = -4624176775548156258L;

	public AddressExportException() {
		super();
	}

	public AddressExportException(String message, Throwable cause) {
		super(message, cause);
	}

	public AddressExportException(String message) {
		super(message);
	}

	public AddressExportException(Throwable cause) {
		super(cause);
	}

}
